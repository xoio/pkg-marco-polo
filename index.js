var npm = require('npm');
var IsThere = require('is-there')

function checkForExistingDependency(filename){
    var target = filename;

    target = target.split("/")[0];

//check to see if the module exists in the node_modules folder
    var exists = IsThere(__dirname + "/../" + target);

    if(exists){
        //stop here
        console.log("Module already installed, aborting.");
        return true;
    }else{
        console.log("file doesn't exist");

        npm.load({
            loaded:false
        },function(err){
            if(!err){
                npm.commands.install([target],function(err,data){
                    if(err){
                        console.log("issue pulling module");
                    }else{
                        console.log("module install successful!")
                    }
                });

                npm.on('log',function(message){
                    console.log(message);
                })
            }
        })

    }
}

module.exports = checkForExistingDependency;