# pkg-marco-polo #

Just a simple node module to check for the existence of a package, and download it if it doesn't already exist in your `node_modules` folder.

Currently not on npm just yet pending further testing.

# To Use #

At the moment, you'll have to clone this to your `node_modules` folder. You should be able to `require` normally after that.

# Future ideas #
* Support pulling from `git` repo if npm module doesn't exist